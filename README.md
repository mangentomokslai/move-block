# README

Files: local.xml

Moves poll block from right to left structural block without reinstantiating block's object.

unsetChild method removes block from specific layout handle, but keeps block's instance.

append method as name suggests appends existing block.
